require('dotenv').config();
const express = require('express');
const path = require('path');
const app = express();
const port = 3001;
const mongoose = require("mongoose");
const Todo = require("./models/TodoScheam.cjs")
const cors = require("cors")
//middleware
// parse requests of content-type - application/json
app.use(cors())
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

main().catch(err => console.log(err));


async function main() {
    await mongoose.connect('mongodb+srv://honghong:honghong@books-data.xk6gxfq.mongodb.net/?retryWrites=true&w=majority&appName=books-data');
    console.log('start of main')
}


app.get('/todo', async (req, res)=> {
    const result = await Todo.find();
    res.json(
        result
    )
})

app.post('/todo', async (req, res)=> {
        const result = await Todo.create({
        title: req.body.title,
        done: false,
        type: req.body.type
    })
    res.json(result)
})

app.delete('/todo/:id', async (req, res)=> {

    const result =  await Todo.findByIdAndDelete(req.params.id);
    res.json(result);
})

app.put("/todo/:id", async (req, res) => {
    var query = {'_id': req.params.id};
    const update = {
        ...req.body
    };

        let results = await Todo.findOneAndUpdate(query, update, {upsert: true});
        results = await Todo.findOne(query);

        res.json({
            success: true,
            data: results,
        });

})

const listener = app.listen( port || process.env.PORT , () => {
    console.log('Server started at http://localhost:' + listener.address().port);
})



import  {useEffect, useState} from 'react'
import {deleteTodo, getTodos, updateCheck, updateTodo} from "./service/todoService.ts"
import {Checkbox, Box, Heading, Flex, Button, useDisclosure, Modal, ModalOverlay, ModalContent, Input} from "@chakra-ui/react";
import CreateForm from "./CreateForm.tsx";
import bg from "./assets/paper.png";
/*
- Create a new Todo, ironia description ------done
- Change the description of a Todo --done
- Complete a Todo -- done
- Delete a Todo --done
- View all Todos  ------view all
- View uncompleted Todos -- done
- View completed Todos -- done
* */
interface Todo {
    _id: string;
   title: string;
   done: boolean;
   type: string;
}

function App() {
    const [data, setData] = useState<Todo[]>([]);
    const [modalData, setModalData] = useState<Todo>({
        _id: "",
        title: "",
        done: false,
        type: ""
    })
    const { isOpen, onOpen, onClose } = useDisclosure();
    const [filter, setFilter] = useState<String>("all") //completed //uncompleted

    const getData = async () => {
        const result = await getTodos();
        setData(result)
    }
    useEffect(() => {
        getData();
    }, []);

    const handleCheck = async(id : string, done : boolean)=> {
        await updateCheck(id, done);
        getData();
    }

    const onChangeHandler = (e: React.FormEvent<HTMLInputElement>) => {
        setModalData({
            ...modalData,
            title: e.currentTarget.value
        })
    }


    const filteredData = data.filter((d: Todo)=>{
        if(filter === "all"){
            return true
        } else if(filter === "completed" && d.done === true){
            return true
        }  else if(filter === "uncompleted" && d.done === false){
            return true
        } else {
            return false;
        }

    })

  return (
    <Box backgroundImage={bg} backgroundSize={"cover"} height={"100vh"}>
      <Box padding={"20px"} fontSize={"20px"} fontWeight={"bold"}>
        <Heading as={"h2"}>Create Todo List</Heading>
        <CreateForm  getData={ getData}/>
          <br></br><Heading as={"h1"}>Todo List</Heading>
          <Flex mt={"20px"} mb={"20px"} gap={"20px"}>
                <Button onClick={()=>setFilter("all")}>View All</Button>
              <Button onClick={()=>setFilter("completed")}>View Completed</Button>
              <Button onClick={()=>setFilter("uncompleted")}>View Uncompleted</Button>
          </Flex>
          <Flex flexDirection={"column"} gap={"10px"}>
          {
              filteredData.map((d: Todo, i : number)=> {
                  return <Flex key={i} gap={"20px"}>
                      <Box>
                      <Checkbox  isChecked={d.done}
                                 onChange={()=>handleCheck(d._id, !d.done)}>{d.title} | {d.type}</Checkbox>
                      </Box>
                      <Flex gap={"20px"}>
                            <Button onClick={async ()=>{
                                await deleteTodo(d._id);
                                await getData();
                            }}>Delete</Button>
                            <Button

                                onClick={()=> {
                                    onOpen()
                                    setModalData({
                                        _id: d._id,
                                        type: d.type,
                                        done: d.done,
                                        title: d.title,
                                    })
                                }}>Update</Button>
                      </Flex>
                  </Flex>
              })
          }
          </Flex>
          

      </Box>

        <Modal isOpen={isOpen} onClose={onClose}>
            <ModalOverlay />
            <ModalContent>
               <Box p="20px">
                   <Input value={modalData.title} onChange={onChangeHandler} ></Input>
                   <Button onClick={async ()=>{
                       await updateTodo(modalData._id, modalData.title);
                       await getData();
                       onClose();
                   }}>Update</Button>
               </Box>
            </ModalContent>
        </Modal>
    </Box>
  )
}

export default App

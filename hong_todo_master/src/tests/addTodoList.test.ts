import {getTodos} from "../service/todoService";
import {createTodo} from "../service/todoService";


test('addTodoList', async () => {
  const testTodo = await getTodos()
  const howManyTodo = Object.keys(testTodo).length
  await createTodo({'title':'what to do', 'type':'morning'})
  const getAllTodo = await getTodos()
  const howManyTodo2= Object.keys(getAllTodo).length;
    expect(howManyTodo2).toBe(howManyTodo + 1);
});






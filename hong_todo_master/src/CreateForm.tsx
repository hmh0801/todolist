
import {Button, FormControl, Box, FormLabel, Input, Select} from "@chakra-ui/react";
import React from "react";
import {createTodo} from "./service/todoService.ts";

interface FormElements extends HTMLFormControlsCollection {
    title: HTMLInputElement
    type: HTMLInputElement
}

interface YourFormElement extends HTMLFormElement {
    readonly elements: FormElements
}

export default function CreateForm ({getData} : {getData: ()=>{}} ) {

    const submitHandler = async (e: React.FormEvent ) => {
        e.preventDefault();
        const target = e.currentTarget as YourFormElement ;
        console.log(target.elements.title.value)
        await createTodo({
            title: target.elements.title.value,
            type: target.elements.type.value
        } )
        getData();

    }

    return <Box as="form" backgroundColor="white" p="20px" onSubmit={submitHandler}>
        <FormControl>
            <FormLabel>Title</FormLabel>
            <Input type='text' name={"title"} />
        </FormControl>
        <FormControl>
            <Select name={"type"} placeholder='Select option' defaultValue={"morning"}>
                <option value='morning'>Morning</option>
                <option value='afternoon'>Afternoon</option>
                <option value='evening'>Evening</option>
            </Select>

        </FormControl>
        <Button>
        <input type="submit" value={"Add Todo"}/>
        </Button>
    </Box>
}
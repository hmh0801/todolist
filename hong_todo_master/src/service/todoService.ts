const PORT = 3001;


export const getTodos = async () => {

   const res = await fetch(`http://localhost:${PORT}/todo`)
   const data = await res.json();
   return data

}

export const createTodo = async (d: { title: string ; type: string }) => {
   
   console.log("createTodo", d.title, d.type)
   const res = await fetch(`http://localhost:${PORT}/todo`, {
      method: "POST",
      headers: {
         "Content-Type": "application/json",
      },
      body: JSON.stringify({
         title: d.title,
         type: d.type
      })
   });
   const data = await res.json();

   return data


}

export const deleteTodo = async (id: string) => {
   const res = await fetch(`http://localhost:${PORT}/todo/${id}`, {
      method: "DELETE",
      headers: {
         "Content-Type": "application/json",
      }
   })
   const data = await res.json();
   console.log(data)

}

export const updateTodo = async(id: string, title: string | null) => {
   const res = await fetch(`http://localhost:${PORT}/todo/${id}`, {
      method: "PUT",
      headers: {
         "Content-Type": "application/json",
      },
      body: JSON.stringify({
         title: title
      })
   })
   await res.json();

}

export const updateCheck = async(id: string, done: boolean) => {
   const res = await fetch(`http://localhost:${PORT}/todo/${id}`, {
      method: "PUT",
      headers: {
         "Content-Type": "application/json",
      },
      body: JSON.stringify({
         done: done
      })
   })
  await res.json();

}